# build with sudo docker build --pull -t centos-unison .
# run with sudo docker run -v $HOME/.unison:$HOME/.unison -v $HOME/.ssh:$HOME/.ssh:ro -v /data:/data -v $HOME/.Xauthority:$HOME/.Xauthority --env="DISPLAY" --net=host --name centos -ti centos-unison bash
FROM centos:7
RUN yum -y install epel-release && \
    yum -y install unison openssh-clients

RUN adduser munzner
USER munzner
